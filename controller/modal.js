
 class Person{
    constructor(hoTen,ma,diaChi,email,loai){
        this.hoTen = hoTen;
        this.ma = ma;
        this.diaChi = diaChi;
        this.email = email;
        
    }
}
 class Student extends Person{
    constructor(hoTen, ma, diaChi, email,toan,ly,hoa){
        super(hoTen, ma, diaChi, email);
        this.toan=toan;
        this.ly=ly;
        this.hoa=hoa;
        this.mangDoiChieu=[this.tinhDTB()]
    }
    tinhDTB(){
        let total=(this.toan+this.ly+this.hoa)/3;
        return total.toFixed(2);
    }

}
 class Employee extends Person{
   constructor(hoTen,ma,diaChi,email,soNgayLamViec,luongTheoNgay){
    super(hoTen,ma ,diaChi, email)
        this.soNgayLamViec=soNgayLamViec;
        this.luongTheoNgay=luongTheoNgay;
   }
}
 class Customer extends Person{
   constructor(hoTen,ma,diaChi,email,tenCongTy,giaHoaDon,danhGia){
    super(hoTen,ma ,diaChi, email)
        this.tenCongTy=tenCongTy;
       this.giaHoaDon=giaHoaDon;
       this.danhGia=danhGia;
   }
}