// import {Employee,Customer,Person,Student } from "./modal";
class ListPerson {
  constructor() {
    this.ListPersons= new Array();
    this.mangDoiChieu=[this.hoTen,this.ma,this.diaChi,this.email,this.toan,this.hoa,this.ly]
  }
  addPerson(newPerson) {
    this.ListPersons = [...this.ListPersons, newPerson];
  }
  findPersonById(ma){
      for(let person of this.ListPersons){
          if(person.ma===ma){
              return person;
          }else{

          }
      }
  }
  findPersonLocation(ma){
    for(var location in this.ListPersons){
        if(this.ListPersons[location].ma===ma){
            return location;
        }
    }
  }
  deletePerson(ma){
  let position=this.findPersonById(ma);
  this.ListPersons.splice(position, 1);

  }
  updatePerson(person){
    let position=this.findPersonLocation(person.ma);
    this.ListPersons[position]=person;
  }

}
